package com.mars.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.ApTest1Request;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 测试1业务层处理
 *
 * @author mars
 * @date 2024-03-01
 */
@Slf4j
@Service
@AllArgsConstructor
public class ApTest1ServiceImpl implements IApTest1Service {

    private final ApTest1Mapper baseMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApTest1 add(ApTest1Request request) {
        ApTest1 entity = ApTest1.builder().build();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(ApTest1Request request) {
        ApTest1 entity = ApTest1.builder().build();
        BeanUtils.copyProperties(request, entity);
        return baseMapper.updateById(entity) > 0 ;
    }

    @Override
    public ApTest1 getById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<ApTest1> pageList(ApTest1Request request) {
        Page<ApTest1> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<ApTest1> query = this.buildWrapper(request);
        IPage<ApTest1> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }


    @Override
    public List<ApTest1> list(ApTest1Request request) {
        LambdaQueryWrapper<ApTest1> query = this.buildWrapper(request);
        return baseMapper.selectList(query);
    }

    private LambdaQueryWrapper<ApTest1> buildWrapper(ApTest1Request param) {
        LambdaQueryWrapper<ApTest1> query = new LambdaQueryWrapper<>();
         if (StringUtils.isNotBlank(param.getName())){
               query.like(ApTest1::getName ,param.getName());
        }
        return query;
    }

}
