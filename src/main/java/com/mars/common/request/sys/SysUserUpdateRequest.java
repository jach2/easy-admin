package com.mars.common.request.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.time.LocalDate;
import java.util.List;

/**
 * 用户修改DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysUserUpdateRequest {

    @NotNull
    @ApiModelProperty(value = "ID")
    private Long id;

    @NotEmpty
    @ApiModelProperty(value = "用户名")
    private String userName;

    @NotEmpty
    @ApiModelProperty(value = "姓名")
    private String realName;

    @NotNull
    @ApiModelProperty(value = "性别（1男  2女）")
    private Integer sex;

    @NotNull
    @ApiModelProperty(value = "出生日期（yyyy-MM-dd）")
    private LocalDate birthDate;

    @NotEmpty
    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "省ID")
    private String province;

    @ApiModelProperty(value = "市ID")
    private String city;

    @ApiModelProperty(value = "区ID")
    private String area;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "角色")
    private List<Long> roleId;

    @ApiModelProperty(value = "岗位ID")
    private Long postId;

}
