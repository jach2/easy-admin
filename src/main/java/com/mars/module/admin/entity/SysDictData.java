package com.mars.module.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.*;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mars.module.system.entity.BaseEntity;

/**
 * 字典数据对象 sys_dict_data
 *
 * @author mars
 * @date 2023-11-18
 */

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@ApiModel(value = "字典数据对象")
@Accessors(chain = true)
@TableName("sys_dict_data")
public class SysDictData extends BaseEntity {


    /**
     * 编码
     */
    @TableId(value = "id" , type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 字典类型ID
     */
    @Excel(name = "字典类型ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "字典类型ID")
    private Long dictTypeId;

    /**
     * 字典类型
     */
    @Excel(name = "字典类型")
    @ApiModelProperty(value = "字典类型")
    private String dictType;

    /**
     * 字典排序
     */
    @Excel(name = "字典排序")
    @ApiModelProperty(value = "字典排序")
    private Integer dictSort;

    /**
     * 字典标签
     */
    @Excel(name = "字典标签")
    @ApiModelProperty(value = "字典标签")
    private String dictLabel;

    /**
     * 字典键值
     */
    @Excel(name = "字典键值")
    @ApiModelProperty(value = "字典键值")
    private String dictValue;
}
