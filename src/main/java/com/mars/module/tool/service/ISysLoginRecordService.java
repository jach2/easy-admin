package com.mars.module.tool.service;

import com.mars.module.tool.entity.SysLoginRecord;
import com.mars.common.response.PageInfo;
import com.mars.module.tool.request.SysLoginRecordRequest;

import java.util.List;

/**
 * 系统访问记录接口
 *
 * @author mars
 * @date 2023-11-17
 */
public interface ISysLoginRecordService {
    /**
     * 新增
     *
     * @param param param
     * @return SysLoginRecord
     */
    SysLoginRecord add(SysLoginRecordRequest param);

    /**
     * 删除
     *
     * @param infoId id
     * @return boolean
     */
    boolean delete(Long infoId);

    /**
     * 批量删除
     *
     * @param infoIds ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> infoIds);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(SysLoginRecordRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param infoId id
     * @return SysLoginRecord
     */
    SysLoginRecord getById(Long infoId);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<SysLoginRecord>
     */
    PageInfo<SysLoginRecord> pageList(SysLoginRecordRequest param);

    /**
     * 在线用户列表
     *
     * @param param 请求参数
     * @return PageInfo<SysLoginRecord>
     */
    PageInfo<SysLoginRecord> onLineUserList(SysLoginRecordRequest param);


}
