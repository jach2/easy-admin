/**
 * 分页查询测试3列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apTest3/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询测试3详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apTest3/query/' + id,
        method: 'get'
    })
}

/**
 * 新增测试3
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apTest3/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改测试3
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apTest3/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除测试3
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apTest3/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出测试3
 *
 * @param query
 * @returns {*}
 */
function exportTest3(query) {
    return requests({
        url: '/admin/apTest3/export',
        method: 'get',
        params: query
    })
}
